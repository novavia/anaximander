#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Init module to Anaximander's registries package.

This module is part of the Anaximander project.
Copyright (C) Novavia Solutions, LLC.
"""

from .folios import *
from .registries import *
